from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone

from .models import Contenido, Comentario

# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')  # Guarda el valor del cuerpo del PUT
    elif request.method == "POST":
        action = request.POST['action']
        if action == "Enviar Contenido":
            valor = request.POST['valor']  # Guarda el valor del formulario de contenidos
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        if request.user.is_authenticated:  # Si no hay login previo, no se modifica el contenido
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()
    if request.method == "POST" and action == "Enviar Comentario":
            c = get_object_or_404(Contenido, clave=llave)
            titulo = request.POST['titulo']
            cuerpo = request.POST['cuerpo']
            q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
            q.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    context = {'contenido': contenido}
    return render(request, 'cms/content.html', context)


def index(request):
    content_list = Contenido.objects.all()
    context = {'content_list': content_list}
    return render(request, 'cms/index.html', context)


def loggedIn(request):
    if request.user.is_authenticated:
        logged = "Logged in as " + request.user.username + '. ' + "<a href='/cms'>Inicio cms</a>"
    else:
        #logged = "Not logged in. <a href='/admin/'>Login via admin</a>"
        logged = "Not logged in. <a href='/login'>Autentícate</a>"
    return HttpResponse(logged)


def logout_view(request):
    logout(request)
    return redirect("/cms/")


def login(request):
    return redirect("/login")
